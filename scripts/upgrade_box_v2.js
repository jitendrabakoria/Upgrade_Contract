// scripts/upgrade_box.js
const { ethers, upgrades } = require("hardhat");

const PROXY = "0xb6d5E8DaEB4e8CFBf0Dd21720B3Fd2b638e4975f";

async function main() {
    const BoxV2 = await ethers.getContractFactory("BoxV2");
    console.log("Upgrading Box...");
    await upgrades.upgradeProxy(PROXY, BoxV2);
    console.log("Box upgraded");
}

main();